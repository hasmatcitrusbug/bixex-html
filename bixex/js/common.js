/* Set navigation */

function openNav() {
  $("#mySidenav").addClass("width80");
  $("#nav-res").addClass("opacityon");
  $(".cd-shadow-layer").addClass("displayblock");
}

function closeNav() {
  $("#mySidenav").removeClass("width80");
  $("#nav-res").removeClass("opacityon");
  $(".cd-shadow-layer").removeClass("displayblock");

} 

$(document).ready(function(){ 

  $(".cd-shadow-layer").click(function(){
    closeNav(); 
  });

  /*  step tab */

  $(".nav-pills a").click(function(){
    $(this).tab('show');
  });
  $('.nav-pills a').on('shown.bs.tab', function(event){
  
  /*  var x = $(event.target).text();         // active tab
    var y = $(event.relatedTarget).text();  // previous tab
    $(".act span").text(x);
    $(".prev span").text(y);
  
    $( ".li-link" ).one( "click", function( event ) {
      $(this).addClass("active");
    }); */

  var href = $(event.target).attr('href');
  var $curr = $(".custom-pills a[href='" + href + "']").parent();

  $('.custom-pills li').removeClass("visited active");

  $curr.addClass("active");
  $curr.prevAll().addClass("visited");

  });

  /* End of step tab */
    
});


$(window).scroll(function(){
  
  var sticky = $('.header-div'),
      scroll = $(window).scrollTop();

    if (scroll >= 100) sticky.addClass('fixed_top');
    else sticky.removeClass('fixed_top');

});

$(window).scroll(function() {
  if ($(this).scrollTop() >= 150) {        
      $('.return-to-top').addClass("display_show");    
  } else {
      $('.return-to-top').removeClass("display_show");   
  }
});
$('.return-to-top').click(function() {    
  $('body,html').animate({
      scrollTop : 0                       
  }, 500);
});