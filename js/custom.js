function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
  document.getElementById("nav-res").style.opacity = "1";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("nav-res").style.opacity = "0";

} 

function openlogout() {
  document.getElementById("mySidenav-logout").style.width = "100%";
  document.getElementById("nav-res").style.opacity = "1";
}

/* Set the width of the side navigation to 0 */
function closelogout() {
  document.getElementById("mySidenav-logout").style.width = "0";
  document.getElementById("nav-res").style.opacity = "0";
  document.getElementById("mySidenav").style.width = "0";
  //$('#nav-res').hide().fadeIn('slow');
} 


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


$(document).ready(function(){
	
    $(".search-icon").click(function(){
        $(".search-filter").slideToggle(500);
    });

    $("#toggle-read").click(function() {
      var elem = $("#toggle-read").text();
      if (elem == "Read More") {
        $("#toggle-read").text("Read Less");
        $("#text_hide_show").show();
      } else {
        $("#toggle-read").text("Read More");
        $("#text_hide_show").hide();
      }
    });

    /*  messages  */

    $(".user-row").click(function(){
      $(".left-4").addClass("left-4none");
      $(".left-8").addClass("left-8show");
    });
    $(".backbtn").click(function(){
      $(".left-4").removeClass("left-4none");
      $(".left-8").removeClass("left-8show");
    });

    $(".search-inputbox, .mobile-input-search").focus(function(){
      $(".search-input-group").addClass("search-input-group-focus");
    });


    /* end of messages */ 


    Waves.attach('.btn', ['waves-effect']);
    

  

});

$(window).scroll(function(){
  if ($(window).scrollTop() >= 290) {
    $('.scroll-position-relative').addClass('scroll-position-fixed');
  }
  else {
    $('.scroll-position-relative').removeClass('scroll-position-fixed');
  }
});


$(window).scroll(function(){
  if ($(window).scrollTop() >= 100) {
    $('.header-top').addClass('fixed-header');
  }
  else {
    $('.header-top').removeClass('fixed-header');
  }
});

$(function() {
  $(".expand").on( "click", function() {
    $expand = $(this).find(">:first-child");
    
    if($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
});
